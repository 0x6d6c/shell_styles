# ZSH prompts colors

autoload -U colors && colors
PROMPT="
%{$fg_bold[blue]%}+%{$reset_color%} %{$fg_no_bold[cyan]%}%M%{$reset_color%}: %{$fg_bold[yellow]%}%~%{$reset_color%} [%*]
%{$fg_no_bold[red]%}%n%{$reset_color%} %{$fg_bold[red]%}#%{$reset_color%} "
RPROMPT="[%{$fg_bold[magenta]%}%?%{$reset_color%}]"
